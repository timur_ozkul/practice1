/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab3UICalculator;



/**
 *
 * @author apprentice
 */
public class Operations {


    public void userInterface()
    {
        System.out.println("What type of operations would you like to do");
        System.out.println("1) Add");
        System.out.println("2) Subtract");
        System.out.println("3) Multiply");
        System.out.println("4) Divide");
        System.out.println("5) Quit");
    }


    public double add(double a, double b) {
        double result = a + b;
        System.out.println("The answer is " + result);
        return result;
        
    }
    
    public double subtract(double a, double b){
        double result = a - b;
        System.out.println("The answer is " + result);
        return result;
    }
    
    public double multiply(double a, double b){
        double result = a * b;
        System.out.println("The answer is " + result);
        return result;
    }
    
    public double divide(double a, double b){
        double result = a / b;
        System.out.println("The answer is " + result);
        return result;
    }
    
}
